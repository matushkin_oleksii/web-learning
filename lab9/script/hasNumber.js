/*
Function checks if variable in parameter contains number
 */

function hasNumber(str) {
    return /\d/.test(str);
}

export {hasNumber}