import {hasNumber} from "../../lab9/script/hasNumber.js";

var indexArray = [1, 'govno', 3, 'davalka', 5, 0.2, 'zalupa', 'penis', 'her', 4.3];

var t2 = indexArray.slice(2, 6)
console.log('Sliced array: ' + t2)

/**
 * Excepting not words from array
 * @type {any[]}
 */
var words = new Array;
for (const aKey in indexArray) {
    if (!hasNumber(indexArray[aKey])) {
        words += indexArray[aKey] + ' '
    }
}

/**
 * Functions finds the shortest word
 * @param s string of words
 * @returns {string} the shortest word and its length
 */
function findShort(s) {
    let res = s.split(' ');
    res.pop()
    let word = '';
    let a = Infinity;

    for (let i = 0; i < res.length; i++) {
        a = Math.min(res[i].length, a);
        word = res[i];
    }
    return a + ' ' + word;
}

console.log('The shortest word: ' + findShort(words))

var assocArray = {'name': 'Alexey', 'age': 19, 'group': 'KN-920b'}
var keys = [];
for (var k in assocArray) keys.push(k);
console.log(keys)
console.log(keys.sort())
console.log('First by alphabet: ' + keys[0])

var skateboard = {
    'Deck': {'size': '8.25', 'cancaves': 'High', 'year': 2021, 'wood': 'Maple'},
    'Wheels': {'rigidity': '100A', 'diameter': 10, 'manufacturer': 'Spitfire'},
    'Abrasive': {'transparency': 0, 'picture': false}
}

var skateKeys = [];
for (var x in skateboard) skateKeys.push(x);
console.log(skateKeys)

for (let i = 0; i < skateKeys.length; i++) {
    var thisParam = skateKeys[i].toString();
    var a = skateKeys[i].toString();
    console.log('key only: ' + a)
    for (k in skateboard[a]) {
        a += '\n' + k + ' - ' + skateboard[thisParam][k] ;
    }
    console.log(a)
}

console.log('App name: ' + navigator.appName)
console.log('App version: ' + navigator.appVersion)